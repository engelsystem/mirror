#!/usr/bin/env ash

set -e

if [[ -z "${TRIGGER_PAYLOAD}" ]] || [[ -z "${GITHUB_TOKEN}" ]] || [[ -z "${GITLAB_TOKEN}" ]] || [[ -z "${LOCAL_REPO}" ]]; then
  echo "Env not configured!"
  exit 1
fi

gitlab() {
  curl -s -H "Authorization: Bearer ${GITLAB_TOKEN}" "https://chaos.expert/api/v4/${1}" ${2}
}

github_status() {
  local gh_url="${1}"
  local pipeline_url="${2}"
  local gitlab_pipeline_status="${3}"
  local github_pipeline_status="${4}"

  curl -s \
    -H "Authorization: Bearer ${GITHUB_TOKEN}" \
    -H "Accept: application/vnd.github+json" \
    -H "X-GitHub-Api-Version: 2022-11-28" \
    "${gh_url}" \
    -X POST \
    -d "{\"state\":\"${github_pipeline_status}\",\"target_url\":\"${pipeline_url}\",\"description\":\"Status: ${gitlab_pipeline_status}\",\"context\":\"ci/gitlab\"}" \
    >/dev/null
}

POLL_INTERVAL=${POLL_INTERVAL-10}
REPO_NAME=$(echo "${LOCAL_REPO}" | sed -e 's/.*://' -e 's/.git//' -e 's~/~%2F~')
COMMIT_SHA=$(echo "${TRIGGER_PAYLOAD}" | jq -r .after)
PROJECT_ID=$(gitlab "projects/${REPO_NAME}" | jq -r .id)
GH_STATUS=$(echo "${TRIGGER_PAYLOAD}" | jq -r .repository.statuses_url | sed -e "s~{sha}~${COMMIT_SHA}~")

if [[ "${COMMIT_SHA}" == "0000000000000000000000000000000000000000" ]]; then
  echo "Got empty commit, skipping.."
  exit 0
fi

echo "Waiting for ${COMMIT_SHA} pipeline"

PIPELINE_ID=''
tries=0
while [[ -z "${PIPELINE_ID}" ]] || [[ 'null' == "${PIPELINE_ID}" ]]; do
  if [[ "${tries}" -gt 6 ]]; then
    echo -e "\nGiving up"
    exit 2
  fi

  echo -n .
  sleep "${POLL_INTERVAL}"

  PIPELINE_ID=$(gitlab "projects/${PROJECT_ID}/pipelines" | jq -r "[ .[] | select(.sha == \"${COMMIT_SHA}\") ] | .[0].id")
  tries=$(($tries + 1))
done

echo
echo "Found pipeline ${PIPELINE_ID}"

PIPELINE_STATUS=''
quit=''
state=''
while [[ -z "${quit}" ]]; do
  PIPELINE=$(gitlab "projects/${PROJECT_ID}/pipelines/${PIPELINE_ID}")
  PIPELINE_STATUS=$(echo "${PIPELINE}" | jq -r .status)
  PIPELINE_URL=$(echo "${PIPELINE}" | jq -r .web_url)

  echo "Status: ${PIPELINE_STATUS}"

  case "${PIPELINE_STATUS}" in

  created | waiting_for_resource | preparing | pending | running | scheduled)
    if [[ "${PIPELINE_STATUS}" != "${state}" ]]; then
      echo "Pending"
      github_status "${GH_STATUS}" "${PIPELINE_URL}" "${PIPELINE_STATUS}" pending
      state="${PIPELINE_STATUS}"
    fi
    ;;

  success | manual | skipped)
    echo "Successfull"
    github_status "${GH_STATUS}" "${PIPELINE_URL}" "${PIPELINE_STATUS}" success
    exit
    ;;

  failed | canceled)
    echo "Failed"
    github_status "${GH_STATUS}" "${PIPELINE_URL}" "${PIPELINE_STATUS}" failure
    exit
    ;;

  esac

  sleep "${POLL_INTERVAL}"
done
