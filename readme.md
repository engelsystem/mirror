# GitHub to GitLab sync
This CI syncs a GitHub repo to GitLab and sets the CI status of the GitHub commit.

# Env config
The GitHub repo needs to trigger a new CI pipeline in this repo on new commits.

| Variable       | Example                            | Description                                                                                                                                                                     |
|----------------|------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `GITHUB_TOKEN` | `github_pat_[…]_[…]`               | A GitHub fine-grained personal access token with `Commit statuses: read and write` access which can be generated with the GitHub project as the resource owner in your settings |
| `GITLAB_TOKEN` | `[…]-[…]`                          | A GitLab token to  access the target repo with `read_api` scope                                                                                                                 |
| `LOCAL_REPO`   | `git@gitlab.example:some/repo.git` | GitLab repo                                                                                                                                                                     |
| `REMOTE_REPO`  | `https://github.com/some/repo.git` | GitHub repo                                                                                                                                                                     |
| `SSH_KEY`      | `-----BEGIN RSA PRIVATE […]`       | SSH deploy key for the target GitLab repo                                                                                                                                       |
